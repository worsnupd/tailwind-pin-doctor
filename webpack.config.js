const webpackMerge = require('webpack-merge');

if (!['dev', 'prod'].includes(process.env.NODE_ENV)) {
    console.error('Invalid or missing NODE_ENV');
    process.exit(1);
}

const commonConfig = require('./config/webpack.common');
const envConfig = require(`./config/webpack.${process.env.NODE_ENV}`);

module.exports = webpackMerge(commonConfig, envConfig);
