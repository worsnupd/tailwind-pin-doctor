const path = require('path');

class Environment {}

Environment.ROOT_DIR = path.resolve(__dirname, '../');
Environment.SOURCE_DIR = path.resolve(Environment.ROOT_DIR, './src');
Environment.SOURCE_STYLES_DIR = path.resolve(Environment.SOURCE_DIR, './styles');
Environment.SOURCE_IMG_DIR = path.resolve(Environment.SOURCE_DIR, './img');
Environment.BUILD_DIR = path.resolve(Environment.ROOT_DIR, './build');
Environment.BUILD_IMG_DIR = path.resolve(Environment.BUILD_DIR, './img');

Environment.APP_ENTRY_POINT = path.resolve(Environment.SOURCE_DIR, './main.ts');
Environment.STYLES_ENTRY_POINT = path.resolve(Environment.SOURCE_STYLES_DIR, './main.scss');
Environment.SOURCE_INDEX_HTML = path.resolve(Environment.SOURCE_DIR, './index.html');

module.exports.Environment = Environment;
