const CopyWebpackPlugin = require('copy-webpack-plugin');
const { Environment } = require('./environment');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');
const ScriptExtHtmlWebpackPlugin = require('script-ext-html-webpack-plugin');
const webpack = require('webpack');

const shouldMinify = process.env.NODE_ENV === 'prod';

module.exports = {
    entry: [
        Environment.APP_ENTRY_POINT,
        Environment.STYLES_ENTRY_POINT
    ],
    output: {
        path: Environment.BUILD_DIR,
        filename: '[name].[hash].js'
    },
    resolve: {
        extensions: ['.ts', '.js', '.json']
    },
    module: {
        loaders: [
            {
                test: /\.ts$/,
                loader: 'ts-loader'
            },
            {
                test: /\.scss$/,
                loader: ExtractTextPlugin.extract('css-loader!sass-loader')
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: Environment.SOURCE_INDEX_HTML,
            minify: {
                minifyJS: shouldMinify,
                minifyCSS: shouldMinify,
                collapseWhitespace: shouldMinify,
                conservativeCollapse: shouldMinify,
                removeComments: shouldMinify
            }
        }),
        new ScriptExtHtmlWebpackPlugin({
            preload: /\.js$/,
            defaultAttribute: 'defer'
        }),
        new ExtractTextPlugin('styles.css'),
        new CopyWebpackPlugin([
            { from: Environment.SOURCE_IMG_DIR, to: Environment.BUILD_IMG_DIR },
        ])
    ]
}
