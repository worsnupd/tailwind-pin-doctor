module.exports = {
    module: {
        loaders: [
            {
                test: /\.template\.html$/,
                loader: 'raw-loader'
            },
        ]
    },
    watch: true,
};
