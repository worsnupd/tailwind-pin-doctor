import { StateErrors } from './constants/state-errors.constant';

runApp.$inject = [
    '$state',
    '$transitions',
];

export function runApp(
    $state: ng.ui.IStateService,
    $transitions: any,
) {
    $transitions.onError({}, (transition) => {
        const error = transition.error().detail;

        switch (error) {
            case StateErrors.NOT_LOGGED_IN:
                $state.go('login');
                return;
            case StateErrors.ALREADY_LOGGED_IN:
                $state.go('home');
                return;
        }

        throw new Error(error);
    });
}
