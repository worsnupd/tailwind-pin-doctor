export const StateErrors = {
    ALREADY_LOGGED_IN: 'ALREADY_LOGGED_IN',
    NOT_LOGGED_IN: 'NOT_LOGGED_IN',
};
