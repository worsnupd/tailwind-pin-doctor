import { LoginController } from '.';

export class LoginComponent implements ng.IComponentOptions {
    public controller = LoginController;
    public template: string = require('./login.template.html');
}
