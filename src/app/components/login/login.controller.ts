import { PinterestService } from '../../services/pinterest';

export class LoginController {
    public static $inject: string[] = ['$state', 'pinterestService'];

    constructor(
        private $state: ng.ui.IStateService,
        private pinterestService: PinterestService,
    ) { ; }

    public async onLoginClick(): Promise<void> {
        await this.pinterestService.login();
        this.$state.go('home');
    }
}
