import { PinReport } from '../../services/pin-analyzer/model/pin-report.model';
import { PinAnalyzerService, PinterestService } from '../../services';
import { PinterestUser, PinterestPin } from '../../services/pinterest/interfaces';

export class HomeController {
    public static $inject: string[] = ['$state', '$scope', 'pinterestService', 'pinAnalyzer'];

    public user: PinterestUser;
    public pinReport:PinReport;

    constructor(
        private $state: ng.ui.IStateService,
        private $scope: ng.IScope,
        private pinterestService: PinterestService,
        private pinAnalyzer: PinAnalyzerService,
    ) { ; }

    public async $onInit(): Promise<void> {
        this.user = await this.pinterestService.getUser();

        this.$scope.$applyAsync();
    }

    public async onAnalyzeClick(): Promise<void> {
        const pins = await this.pinterestService.getPins();
        this.pinReport = await this.pinAnalyzer.analyzePins(pins);

        this.$scope.$applyAsync();
    }

    public onLogoutClick(): void {
        this.pinterestService.logout();
        this.$state.go('login');
    }
}
