import { HomeController } from '.';

export class HomeComponent implements ng.IComponentOptions {
    public controller = HomeController;
    public template: string = require('./home.template.html');
}
