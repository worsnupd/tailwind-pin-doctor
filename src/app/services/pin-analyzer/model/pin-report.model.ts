import { PinterestPin } from '../..';
import { PinIssue } from './pin-issue';

export class PinReport {
    public issues: PinIssue[] = [];
    private _failedPins: Set<PinterestPin> = new Set<PinterestPin>();
    private _passedPins: Set<PinterestPin>;

    constructor(
        public pins: PinterestPin[]
    ) {
        this._passedPins = new Set(pins);
    }

    public get failedPins(): PinterestPin[] {
        return Array.from(this._failedPins);
    }

    public get passedPins(): PinterestPin[] {
        return Array.from(this._passedPins);
    }

    public addIssues(issues: PinIssue[]): void {
        for (const issue of issues) {
            this._failedPins.add(issue.pin);
            this._passedPins.delete(issue.pin);
            this.issues.push(issue);
        }
    }
}
