import { PinterestPin } from '../../../pinterest/interfaces/pinterest-pin.interface';
import { PinIssue } from '.';

export class BrokenLinkPinIssue extends PinIssue {
    constructor(
        public pin: PinterestPin,
    ) {
        super(pin, `Broken link: ${pin.image.original.url}`);
    }
}
