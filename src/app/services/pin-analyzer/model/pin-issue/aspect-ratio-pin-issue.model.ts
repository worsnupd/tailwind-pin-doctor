import { PinterestPin } from '../../../pinterest/interfaces/pinterest-pin.interface';
import { PinIssue } from '.';

export class AspectRatioPinIssue extends PinIssue {
    constructor(
        public pin: PinterestPin,
    ) {
        super(pin, `Bad aspect ratio: ${pin.image.original.width}x${pin.image.original.height}`);
    }
}
