import { PinterestPin } from '../../../pinterest/interfaces/pinterest-pin.interface';

export abstract class PinIssue {
    constructor(
        public pin: PinterestPin,
        public message: string,
    ) { ; }
}
