import { PinterestPin } from '../../../pinterest/interfaces/pinterest-pin.interface';
import { PinIssue } from '.';

export class DuplicatePinIssue extends PinIssue {
    constructor(
        public pin: PinterestPin,
        public otherPin: PinterestPin,
    ) {
        super(pin, `Duplicate image: same image as pin ${otherPin.id}`);
    }
}
