import { PinterestPin } from '../pinterest/interfaces/pinterest-pin.interface';
import {
    PinReport,
    AspectRatioPinIssue,
    BrokenLinkPinIssue,
    DuplicatePinIssue,
} from './model';

export class PinAnalyzerService {
    public async analyzePins(pins: PinterestPin[]): Promise<PinReport> {
        const report = new PinReport(pins);

        const aspectRatioIssues = this.checkForBadAspectRatios(pins);
        const duplicateIssues = this.checkForDuplicates(pins);
        const brokenLinkIssues = await this.checkForBrokenLinks(pins);

        report.addIssues(aspectRatioIssues);
        report.addIssues(brokenLinkIssues);
        report.addIssues(duplicateIssues);

        return report;
    }

    private checkForBadAspectRatios(pins: PinterestPin[]): AspectRatioPinIssue[] {
        return pins
            .filter(pin => pin.image.original.width > pin.image.original.height)
            .map(pin => new AspectRatioPinIssue(pin));
    }

    private async checkForBrokenLinks(pins: PinterestPin[]): Promise<DuplicatePinIssue[]> {
        const issues = [];

        return issues;
    }

    private checkForDuplicates(pins: PinterestPin[]): DuplicatePinIssue[] {
        const imageUrlsMap = new Map<string, PinterestPin>();
        const issues = [];

        for (const pin of pins) {
            const otherPin = imageUrlsMap.get(pin.image.original.url);

            if (otherPin) {
                issues.push(new DuplicatePinIssue(pin, otherPin));
            } else {
                imageUrlsMap.set(pin.image.original.url, pin);
            }
        }

        return issues;
    }
}
