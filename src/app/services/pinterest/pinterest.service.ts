import {
    PDK,
    PinterestSession,
    PinterestUser,
    PinterestLoginResponse,
    PinterestPin,
} from '.';

export class PinterestService {
    public static $inject: string[] = ['$window'];

    private APP_ID: string = '4928595390294403422';
    private pdk: PDK;

    constructor(
        $window: ng.IWindowService,
    ) {
        this.pdk = $window['PDK'];

        this.pdk.init({
            appId: this.APP_ID,
            cookie: true
        });
    }

    public get session(): PinterestSession {
        return this.pdk.getSession();
    }

    public set session(newSession: PinterestSession) {
        this.pdk.setSession(newSession);
    }

    public isLoggedIn(): boolean {
        return !!this.session;
    }

    public login(): Promise<PinterestSession> {
        return new Promise((resolve, reject) => {
            this.pdk.login({ scope: 'read_public' }, resolve);
        });
    }

    public logout(): void {
        this.pdk.logout();
    }

    public getUser(): Promise<PinterestUser> {
        return new Promise((resolve, reject) => {
            this.pdk.me(response => {
                resolve(response.data);
            });
        });
    }

    public getPins(): Promise<PinterestPin[]> {
        const params = {
            fields: [
                'id',
                'link',
                'note',
                'url',
                'creator',
                'board',
                'created_at',
                'color',
                'counts',
                'media',
                'attribution',
                'image',
                'metadata',
            ].join(','),
        };

        return new Promise((resolve, reject) => {
            this.pdk.me('pins', params, response => {
                resolve(response.data);
            });
        });
    }
}
