import { PinterestPinImage } from '.';

export interface PinterestPin {
    id: string;
    link: string;
    note: string;
    url: string;
    creator: any;
    board: any;
    created_at: string;
    color: string;
    counts: any;
    media: any;
    attribution: any;
    image: PinterestPinImage;
    metadata: any;
}
