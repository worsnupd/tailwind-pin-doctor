import { PinterestSession } from '.';

export interface PinterestLoginResponse {
    session: PinterestSession;
}
