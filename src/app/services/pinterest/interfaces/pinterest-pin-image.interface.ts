export interface PinterestPinImage {
    original: { width: number, height: number, url: string };
}
