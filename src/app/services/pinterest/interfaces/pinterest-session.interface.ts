export interface PinterestSession {
    accessToken: string;
    scope: string;
}
