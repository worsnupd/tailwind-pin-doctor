import { PinterestSession } from '.';

export interface PDK {
    init: Function;
    login: Function;
    logout: Function;
    getSession: () => PinterestSession;
    setSession: Function;
    pin: Function;
    me: Function;
    request: Function;
}
