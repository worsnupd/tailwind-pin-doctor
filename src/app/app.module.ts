import { PinAnalyzerService } from './services/pin-analyzer';
import { configureStates } from './states';
import { HomeComponent } from './components/home';
import { LoginComponent } from './components/login';
import { PinterestService } from './services/pinterest';
import { runApp } from './app.run';

const appModule: ng.IModule = angular
    .module('app', [
        'ui.router',
        'ngMaterial'
    ])
    .config(configureStates)
    .run(runApp)
    .component('home', new HomeComponent())
    .component('login', new LoginComponent())
    .service('pinterestService', PinterestService)
    .service('pinAnalyzer', PinAnalyzerService);;
