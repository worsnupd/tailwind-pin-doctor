import { PinterestService } from '../services/pinterest';
import { StateErrors } from '../constants/state-errors.constant';

export const HOME: ng.ui.IState = {
    url: '/',
    views: {
        'content@': {
            component: 'home'
        },
    },
    resolve: {
        sessionCheck: ['pinterestService', (
            pinterestService: PinterestService,
        ) => {
            if (!pinterestService.isLoggedIn()) {
                return Promise.reject(StateErrors.NOT_LOGGED_IN);
            }
        }],
    },
};
