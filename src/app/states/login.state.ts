import { PinterestService } from '../services/pinterest';
import { StateErrors } from '../constants/state-errors.constant';

export const LOGIN: ng.ui.IState = {
    url: '/login',
    views: {
        'content@': {
            component: 'login'
        },
    },
    resolve: {
        sessionCheck: ['pinterestService', (
            pinterestService: PinterestService,
        ) => {
            if (pinterestService.isLoggedIn()) {
                return Promise.reject(StateErrors.ALREADY_LOGGED_IN);
            }
        }],
    },
};
