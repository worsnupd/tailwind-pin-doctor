import { HOME } from './home.state';
import { LOGIN } from './login.state';

configureStates.$inject = [
    '$stateProvider',
    '$locationProvider',
];

export function configureStates(
    $stateProvider: ng.ui.IStateProvider,
    $locationProvider: ng.ILocationProvider,
) {
    $locationProvider.html5Mode({
        enabled: true,
        requireBase: false,
    });

    $locationProvider.hashPrefix('!');



    $stateProvider.state('home', HOME);
    $stateProvider.state('login', LOGIN);
}
