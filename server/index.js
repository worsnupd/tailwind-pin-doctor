const express = require('express');
const fs = require('fs');
const https = require('https');
const path = require('path');

const checkLink = require('./api/checkLink').default;

const app = express();
const buildDir = path.join(__dirname, '../build');
const buildImgDir = path.join(buildDir, './img');
const contentTypeMap = {
    css: 'text/css',
    js: 'application/javascript'
};

app.use('/img', express.static(buildImgDir));

app.get(/\/api\/checkLink(\?.+)?/, checkLink);

app.get(/.*\.(css|js)$/, (req, res) => {
    const fileExtension = /.*\.(css|js)$/.exec(req.originalUrl)[1];
    const fileContents = fs.readFileSync(path.join(buildDir, req.originalUrl), 'utf-8');
    const headers = {
        'Content-Type': `${contentTypeMap[fileExtension]}; charset=utf-8`
    };

    res.header(headers).send(fileContents);
});

app.get(/.*/, (req, res) => {
    const indexFile = fs.readFileSync(path.join(buildDir, 'index.html'), 'utf-8');
    const headers = {
        'Content-Type': 'text/html; charset=utf-8'
    };

    res.header(headers).send(indexFile);
});

const options = {
    key: fs.readFileSync(process.env.SSL_KEY_FILE),
    cert: fs.readFileSync(process.env.SSL_CERT_FILE),
    requestCert: false,
    rejectUnauthorized: false,
};

const server = https.createServer(options, app).listen(3005, () => {
    console.log('Server listening on port 3005');
});
