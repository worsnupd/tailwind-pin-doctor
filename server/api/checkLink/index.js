const url = require('url');
const validUrl = require('valid-url');
const request = require('request');
const dnsLookup = require('dns-lookup');

exports.default = (req, res) => {
    let link = req.query.link;

    if (!link) {
        res.status(400).send('No link parameter provided.');
        return;
    }

    if (!/^https?:\/\//.test(link)) {
        link = 'http://' + link;
    }

    const urlParsed = url.parse(link);

    if (!validUrl.isWebUri(link)) {
        res.status(400).send('Invalid link parameter provided');
        return;
    }

    dnsLookup(urlParsed.host, function (err, address, family) {
        if (err) {
            res.status(404).send('Broken Link');
            return;
        }

        request(link, (error, response) => {
            if (error) {
                res.status(500).send(`Error: ${error.message}`);
                return;
            }

            if (response && response.statusCode >= 200 && response.statusCode <= 299) {
                res.send('OK');
                return;
            }

            res.status(404).send('Broken Link');
        });
    });
};
